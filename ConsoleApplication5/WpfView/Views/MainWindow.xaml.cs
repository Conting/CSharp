﻿using System.Windows;
using Common.Services.Commands;
using Common.ViewModel;
using log4net.Config;

namespace WpfView.Views
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += OnLoaded;
            XmlConfigurator.Configure();
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            DataContext = new MainWindowViewModel(new DialogService(this));
        }
    }
}