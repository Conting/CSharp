﻿using System;
using System.IO;

namespace Common.Commons
{
    /// <summary>
    ///     Constant class for dialogs, error messages etc.
    /// </summary>
    internal class Constants
    {
        /// <summary>
        ///     Password string pools
        /// </summary>
        public static class Passwords
        {
            public static string PASSWORD_CHARS_LCASE = "abcdefgijkmnopqrstwxyz";
            public static string PASSWORD_CHARS_UCASE = "ABCDEFGHJKLMNPQRSTWXYZ";
            public static string PASSWORD_CHARS_NUMERIC = "23456789";
            public static string PASSWORD_CHARS_SPECIAL = "*$-+?_&=!%{}/";
        }

        /// <summary>
        ///     Directory paths
        /// </summary>
        public static class Directories
        {
            /// <summary>
            ///     Gets the local directory.
            /// </summary>
            /// <returns></returns>
            public static string GET_LOCAL_DIRECTORY()
            {
                var path =
                    Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)).FullName;
                if (Environment.OSVersion.Version.Major >= 6)
                    path = Directory.GetParent(path).ToString();
                path = path + @"\AppData\Local";

                return path;
            }

            /// <summary>
            ///     Gets the personal folder.
            /// </summary>
            /// <returns></returns>
            public static string GET_PERSONAL_FOLDER()
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            }
        }


        /// <summary>
        ///     Error messages
        /// </summary>
        public static class Errors
        {
            public static string ENCRYPTION_ERROR = "Encryption Error!";
            public static string DECRYPTION_ERROR = "Decryption Error!";
            public static string SOURCE_IS_SAME_AS_DEST = "Source folder cannot be the same as decryption folder!";
            public static string SOURCE_IS_EMPTY_OR_WHITESPACE = "Source path cannot be empty";
            public static string DEST_IS_EMPTY_OR_WHITESPACE = "Destination path cannot be empty";
            public static string SOURCE_IS_NOT_VALID = "Source path is not valid";
            public static string DEST_IS_NOT_VALID = "Destination path is not valid";
            public static string PASSWORD_IS_NULL_OR_WHITESPACE = "Password cannot be empty";
            public static string PASSWORDS_ARE_NOT_THE_SAME = "Password are not the same!";
        }

        /// <summary>
        ///     Warning messages
        /// </summary>
        public static class WARNINGS
        {
            public static string ENCRYPTION_WARNING = "FILES WILL BE ENCRYPTED!";
            public static string DECRYPTION_WARNING = "FILES WILL BE DECRYPTED!";
        }

        /// <summary>
        ///     Question messages
        /// </summary>
        public static class QUESTIONS
        {
            public static string ENCRYPTION_QUESTION = "Are you sure that you want to encrypt those files?";
            public static string DECRPYTION_QUESTION = "Are you sure that you want to decrypt those files?";
        }
    }
}