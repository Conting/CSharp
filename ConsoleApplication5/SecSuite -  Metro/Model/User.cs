﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Common.Model
{
    /// <summary>
    ///     Stores credentials required by the process which a user accesses to a computer system.
    /// </summary>
    /// <seealso cref="System.ComponentModel.INotifyPropertyChanged" />
    [Serializable]
    public class User : INotifyPropertyChanged
    {
        private List<AdditionalInfo> _additionalInfos;
        private string _categories;
        private DateTime _created;
        private DateTime _modified;
        private string _password;
        private string _title;
        private string _username;
        private string _website;

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        /// <value>
        ///     The title.
        /// </value>
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the username.
        /// </summary>
        /// <value>
        ///     The username.
        /// </value>
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the password.
        /// </summary>
        /// <value>
        ///     The password.
        /// </value>
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the website.
        /// </summary>
        /// <value>
        ///     The website.
        /// </value>
        public string Website
        {
            get { return _website; }
            set
            {
                _website = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the creation date.
        /// </summary>
        /// <value>
        ///     The created.
        /// </value>
        public DateTime Created
        {
            get { return _created; }
            set
            {
                _created = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the modified date.
        /// </summary>
        /// <value>
        ///     The modified.
        /// </value>
        public DateTime Modified
        {
            get { return _modified; }
            set
            {
                _modified = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the additional infos.
        /// </summary>
        /// <value>
        ///     The additional infos.
        /// </value>
        public List<AdditionalInfo> AdditionalInfos
        {
            get { return _additionalInfos; }
            set
            {
                _additionalInfos = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the category.
        /// </summary>
        /// <value>
        ///     The categories.
        /// </value>
        public string Categories
        {
            get { return _categories; }

            set { _categories = value; }
        }

        /// <summary>
        ///     Occurs when a property value changes.
        /// </summary>
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        ///     Gets the user list.
        /// </summary>
        /// <param name="list">The list to covert to a ObservableCollection.</param>
        /// <returns></returns>
        public ObservableCollection<User> GetUserList(List<User> list)
        {
            var users = new ObservableCollection<User>();
            foreach (var user in list)
                users.Add(user);
            return users;
        }
    }
}