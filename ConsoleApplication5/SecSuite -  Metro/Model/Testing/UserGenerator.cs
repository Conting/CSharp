﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Common.Commons;
using Common.Services.Pwgenerator;
using Common.Services.SecureNumberGenerator;

namespace Common.Model.Testing
{
    /// <summary>
    ///     User Object generator for testing purposes
    /// </summary>
    internal class UserGenerator
    {
        private const string Website = "http://the-conblog.com";
        private const string Title = "Best Site Ever";
        private readonly List<AdditionalInfo> _additionalInfos = new List<AdditionalInfo>();
        private readonly List<string> _constants = new List<string>();
        private readonly PwGenerator _generator = new PwGenerator();
        private readonly string[] _names = {"JAMES", "JOHN", "ROBERT", "MICHAEL", "WILLIAM"};
        private readonly SecureNumber _secureNumber = new SecureNumber();


        /// <summary>
        ///     User generator method.
        /// </summary>
        /// <returns>List of Users</returns>
        public ObservableCollection<User> Samples()
        {
            var toReturn = new ObservableCollection<User>();

            _constants.Add(Constants.Passwords.PASSWORD_CHARS_LCASE);
            _constants.Add(Constants.Passwords.PASSWORD_CHARS_NUMERIC);
            _constants.Add(Constants.Passwords.PASSWORD_CHARS_UCASE);
            _constants.Add(Constants.Passwords.PASSWORD_CHARS_SPECIAL);
            for (var i = 0; i < 100; i++)
            {
                for (var j = 0; j < 10; j++)
                {
                    var add = new AdditionalInfo
                    {
                        Name = Title,
                        Value = _names[_secureNumber.Next(0, 5)]
                    };
                    _additionalInfos.Add(add);
                }
                toReturn.Add(new User
                {
                    Username = _names[_secureNumber.Next(0, 5)],
                    Password = _generator.GeneratePassword(20, _constants),
                    Title = Title,
                    Website = Website,
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    AdditionalInfos = _additionalInfos
                });
            }
            return toReturn;
        }
    }
}