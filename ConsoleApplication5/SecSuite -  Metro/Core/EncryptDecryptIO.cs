﻿using System.IO;
using System.Linq;
using Common.Helpers;
using Common.Services;
using Common.Services.Crypto;
using Common.Services.FileCopier;

namespace Common.Core
{
    /// <summary>
    ///     Provides methods for a whole encryption and decryption process.
    /// </summary>
    public class EncryptDecryptIo
    {
        private readonly AbstractSymmetricCryptoService _aesCryptoService = new AbstractSymmetricCryptoService();
        private readonly DirectoryIterator _directoryIterator = new DirectoryIterator();
        private readonly FileCopier _fileCopier = new FileCopier();
        private readonly FileLockChecker _fileLockChecker = new FileLockChecker();

        /// <summary>
        ///     Encrypts the input to the output path with the use of the password.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="output">The output.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public bool Encrypt(string input, string output, string password)
        {
            if (input.Equals(output)) return false;
            //Return input directory
            InfoUpdater.Provider.Status = "Getting data information ...";

            var inputDirectoryList = _directoryIterator.RetDirectoryList(input);
            //Return file list
            var inputFileList = _directoryIterator.RetFileList(input);
            InfoUpdater.Provider.Status = "Check file locks ...";

            //Check if files  are locked or null
            if (string.IsNullOrWhiteSpace(input) || string.IsNullOrWhiteSpace(output)) return false;
            if (inputFileList.Any(file => _fileLockChecker.IsFileLocked(new FileInfo(file))))
            {
                InfoUpdater.Provider.Status = "Files are locked.";
                return false;
            }
            //Copy directories and files
            InfoUpdater.Provider.Status = "Copy files ...";

            if (inputDirectoryList.Any(directory => !_fileCopier.CopyDirectories(input, directory, output)))
            {
                InfoUpdater.Provider.Status = "Could not copy directories.";

                return false;
            }
            if (inputFileList.Any(files => !_fileCopier.CopyFiles(input, files, output)))
            {
                InfoUpdater.Provider.Status = "Could not copy files.";

                return false;
            }
            InfoUpdater.Provider.Status = "Encrypt files ...";

            //Encrypt files
            var outputFileList = _directoryIterator.RetFileList(output);
            outputFileList.RemoveAll(EndsWithDotConcrypt);
            if (outputFileList.Any(files => !_aesCryptoService.AES_Encrypt(files, password, ".concrypt")))
            {
                InfoUpdater.Provider.Status = "Files could not be encrypted";

                return false;
            }
            //Delete old files
            InfoUpdater.Provider.Status = "Cleanup locks ...";

            foreach (var oldFile in outputFileList)
                if (_fileLockChecker.IsFileLocked(new FileInfo(oldFile)))
                {
                    InfoUpdater.Provider.Status = "Error: Files are locked";
                    return false;
                }
            InfoUpdater.Provider.Status = "Files encryted";

            return true;
        }

        /// <summary>
        ///     Decrypts the input to the output path with the password.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="output">The output.</param>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public bool Decrypt(string input, string output, string password)
        {
            if (input.Equals(output)) return false;
            InfoUpdater.Provider.Status = "Get file information ...";
            //Return input directory
            var inputDirectoryList = _directoryIterator.RetDirectoryList(input);
            //Return file list
            var inputFileList = _directoryIterator.RetFileList(input);
            //Check if files  are locked or null
            if (string.IsNullOrWhiteSpace(input) || string.IsNullOrWhiteSpace(output)) return false;
            InfoUpdater.Provider.Status = "Check file locks ...";

            if (inputFileList.Any(file => _fileLockChecker.IsFileLocked(new FileInfo(file))))
            {
                InfoUpdater.Provider.Status = "Files are locked";
                return false;
            }
            //Copy directories and files
            InfoUpdater.Provider.Status = "Copy directorries ...";

            if (inputDirectoryList.Any(directory => !_fileCopier.CopyDirectories(input, directory, output)))
            {
                InfoUpdater.Provider.Status = "Could not copy directories.";
                return false;
            }
            InfoUpdater.Provider.Status = "Copy files ...";
            if (inputFileList.Any(files => !_fileCopier.CopyFiles(input, files, output)))
            {
                InfoUpdater.Provider.Status = "Could not copy files.";
                return false;
            }
            var outputFileList = _directoryIterator.RetFileList(output);
            //Delete every non conrypt file out of outputFileList
            outputFileList.RemoveAll(NotEndsWithDotConcrypt);
            //Encrypt files
            InfoUpdater.Provider.Status = "Decrypt files ...";

            if (outputFileList.Any(files => !_aesCryptoService.AES_Decrypt(files, password, output)))
            {
                InfoUpdater.Provider.Status = "Could not decrypt files.";
                return false;
            }
            //Delete old files
            InfoUpdater.Provider.Status = "Cleanup ...";

            foreach (var oldFile in outputFileList)
                File.Delete(oldFile);

            InfoUpdater.Provider.Status = "Files decryted.";
            return true;
        }

        /// <summary>
        ///     Boolean which specifies if file does not ends with .concrypt
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns></returns>
        private static bool NotEndsWithDotConcrypt(string file)
        {
            return !file.ToLower().EndsWith(".concrypt");
        }

        /// <summary>
        ///     Boolean which specifies if the file ends with .concrypt
        /// </summary>
        /// <param name="file">file with path.</param>
        /// <returns></returns>
        private static bool EndsWithDotConcrypt(string file)
        {
            return file.ToLower().EndsWith(".concrypt");
        }
    }
}