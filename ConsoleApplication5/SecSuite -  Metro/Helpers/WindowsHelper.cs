﻿using System.Windows;
using System.Windows.Input;

namespace Common.Helpers
{
    /// <summary>
    ///     Provides properties to access to windows in a mvvm application.
    /// </summary>
    public class WindowsHelper
    {
        /// <summary>
        ///     The run on window closing property
        /// </summary>
        public static readonly DependencyProperty RunOnWindowClosingProperty =
            DependencyProperty.RegisterAttached(
                "RunOnWindowClosing",
                typeof(ICommand),
                typeof(WindowsHelper),
                new PropertyMetadata(default(ICommand), RunOnWindowClosingPropertyChangedCallback));

        /// <summary>
        ///     Runs the on window closing property changed callback.
        /// </summary>
        /// <param name="dependencyObject">The dependency object.</param>
        /// <param name="dependencyPropertyChangedEventArgs">
        ///     The <see cref="DependencyPropertyChangedEventArgs" /> instance
        ///     containing the event data.
        /// </param>
        private static void RunOnWindowClosingPropertyChangedCallback(DependencyObject dependencyObject,
            DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            var window = dependencyObject as Window;
            if (window != null)
                window.Closing += (sender, args) =>
                {
                    if (GetRunOnWindowClosing(window) == null)
                        return;
                    args.Cancel = true;
                    GetRunOnWindowClosing(window)?.Execute(dependencyObject);
                };
        }

        /// <summary>
        ///     Sets the run on window closing.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="value">The value.</param>
        public static void SetRunOnWindowClosing(DependencyObject element, ICommand value)
        {
            element.SetValue(RunOnWindowClosingProperty, value);
        }

        /// <summary>
        ///     Gets the run on window closing.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <returns></returns>
        public static ICommand GetRunOnWindowClosing(DependencyObject element)
        {
            return (ICommand) element.GetValue(RunOnWindowClosingProperty);
        }
    }
}