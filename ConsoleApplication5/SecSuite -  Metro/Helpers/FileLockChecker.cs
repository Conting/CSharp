﻿using System;
using System.IO;
using System.Reflection;
using log4net;

namespace Common.Helpers
{
    /// <summary>
    ///     Provides method to check if files or folders are locked.
    /// </summary>
    internal class FileLockChecker
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        ///     Determines whether the file is locked or not.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>
        ///     <c>true</c> if is locked; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException e)
            {
                _log.Error(e.Message);
                _log.Error(e.InnerException);
                return true;
            }
            catch (UnauthorizedAccessException e)
            {
                _log.Error(e.Message);
                _log.Error(e.InnerException);
                return true;
            }
            finally
            {
                stream?.Close();
            }
            return false;
        }
    }
}