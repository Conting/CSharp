﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Common.Helpers
{
    /// <summary>
    ///     Provides methods to get all files and subdirectory paths of a directory as a list of strings.
    /// </summary>
    internal class DirectoryIterator
    {
        /// <summary>
        ///     Returns the directory list from input.
        /// </summary>
        /// <param name="input">The input path.</param>
        /// <returns></returns>
        public List<string> RetDirectoryList(string input)
        {
            var directories = Directory.GetDirectories(input, "*.*", SearchOption.AllDirectories).ToList();
            directories = directories.Distinct().ToList();
            return directories;
        }

        /// <summary>
        ///     Returns the file list from input.
        /// </summary>
        /// <param name="input">The input path.</param>
        /// <returns></returns>
        public List<string> RetFileList(string input)
        {
            var files =
                Directory.GetFiles(input, "*.*", SearchOption.AllDirectories).ToList();
            files = files.Distinct().ToList();
            return files;
        }
    }
}