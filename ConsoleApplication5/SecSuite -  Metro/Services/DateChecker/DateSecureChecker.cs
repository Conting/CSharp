﻿using System;
using Common.Model;

namespace Common.Services.DateChecker
{
    /// <summary>
    ///     DateSecureChecker is a class for verifing if dates are in the past.
    /// </summary>
    public class DateSecureChecker
    {
        /// <summary>
        ///     Checks if date is 90 days back.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public bool CheckIfDateIs90DaysBack(User user)
        {
            return (user.Modified - DateTime.Now).TotalDays > 90;
        }

        /// <summary>
        ///     Checks if date is x days back.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="x">The number of days.</param>
        /// <returns></returns>
        public bool CheckIfDateIsXDaysBack(User user, int x)
        {
            return (user.Modified - DateTime.Now).TotalDays > x;
        }
    }
}