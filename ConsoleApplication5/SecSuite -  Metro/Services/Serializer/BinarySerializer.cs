﻿using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using log4net;

namespace Common.Services.Serializer
{
    /// <summary>
    ///     BinarySerializer translates data structures or object into a format, that can be stored in a file.
    /// </summary>
    public class BinarySerializer
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        ///     Serializes the specified input in a file at the filename path.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="input">The input.</param>
        /// <param name="filename">The filename path.</param>
        /// <returns></returns>
        public bool Serialize<T>(T input, string filename)
        {
            var fs = new FileStream(filename, FileMode.OpenOrCreate);

            var formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, input);
            }
            catch (SerializationException e)
            {
                _log.Error(e.Message);
                _log.Error(e.InnerException);
                return false;
            }
            finally
            {
                fs.Close();
            }
            return true;
        }


        /// <summary>
        ///     Deserializes a file with specified filename path.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filename">The filename.</param>
        /// <returns></returns>
        public T Deserialize<T>(string filename)
        {
            FileStream fs;
            var toReturn = default(T);
            try
            {
                fs = new FileStream(filename, FileMode.Open);
            }
            catch (FileNotFoundException e)
            {
                _log.Error(e.Message);
                _log.Error(e.InnerException);
                return toReturn;
            }

            try
            {
                var formatter = new BinaryFormatter();
                toReturn = (T) formatter.Deserialize(fs);
                return toReturn;
            }
            catch (SerializationException e)
            {
                _log.Error(e.Message);
                _log.Error(e.InnerException);
                return toReturn;
            }
            finally
            {
                fs.Close();
            }
        }
    }
}