﻿using System.Collections.Generic;
using Common.Services.SecureNumberGenerator;

namespace Common.Services.Pwgenerator
{
    /// <summary>
    ///     PwGeneraor that takes input from a pseudo-random number generator and automatically
    ///     generates a password from a character string pool.
    /// </summary>
    public class PwGenerator
    {
        private readonly SecureNumber _sc = new SecureNumber();


        /// <summary>
        ///     Generates a password.
        /// </summary>
        /// <param name="length">The password length.</param>
        /// <param name="list">Character pool list</param>
        /// <returns></returns>
        public string GeneratePassword(int length, List<string> list)
        {
            var temp = new char[length];
            for (var i = 0; i < length; ++i)
            {
                var str = list[_sc.Next(0, list.Count)];
                temp[i] = str[_sc.Next(0, str.Length)];
            }
            var toReturn = new string(temp);
            return toReturn;
        }
    }
}