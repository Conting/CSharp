﻿using System.Threading.Tasks;
using MahApps.Metro.Controls.Dialogs;

namespace Common.Services.Commands
{
    /// <summary>
    ///     Provides methods to show a metro dialog window.
    /// </summary>
    public interface IDialogService
    {
        Task<MessageDialogResult> AskQuestionAsync(string title, string message);
        Task<ProgressDialogController> ShowProgressAsync(string title, string message);
        Task ShowMessageAsync(string title, string message);
        string ShowOpenDirectoryDialog(string title, string initialDirectory);
        string ShowSaveFileDialog(string title, string initialDirectory, string extensionName, string extension);
        string ShowOpenFileDialog(string title, string initialDirectory, string extensionName, string extension);
    }
}