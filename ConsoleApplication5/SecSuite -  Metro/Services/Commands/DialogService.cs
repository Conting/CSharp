﻿using System;
using System.Threading.Tasks;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace Common.Services.Commands
{
    /// <summary>
    ///     DialogService is a IDialogService implementation that allows you to show a metro dialog.
    /// </summary>
    /// <seealso cref="Common.Services.Commands.IDialogService" />
    public class DialogService : IDialogService
    {
        private readonly MetroWindow _metroWindow;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DialogService" /> class.
        /// </summary>
        /// <param name="metroWindow">The metro window.</param>
        public DialogService(MetroWindow metroWindow)
        {
            _metroWindow = metroWindow;
        }


        /// <summary>
        ///     Show a asynchronous metro AskQuestion-Dialog.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public Task<MessageDialogResult> AskQuestionAsync(string title, string message)
        {
            var settings = new MetroDialogSettings
            {
                AffirmativeButtonText = "Yes",
                NegativeButtonText = "No"
            };
            return _metroWindow.Dispatcher.Invoke(() => _metroWindow.ShowMessageAsync(title, message,
                MessageDialogStyle.AffirmativeAndNegative, settings));
        }

        /// <summary>
        ///     Shows the metro progress dialog asynchronous.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public Task<ProgressDialogController> ShowProgressAsync(string title, string message)
        {
            return _metroWindow.ShowProgressAsync(title, message);
        }

        /// <summary>
        ///     Shows the metro message dialog asynchronous.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public Task ShowMessageAsync(string title, string message)
            => _metroWindow.Dispatcher.Invoke(() => _metroWindow.ShowMessageAsync(title, message));

        /// <summary>
        /// Shows the open directory dialog.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="initialDirectory">The initial directory.</param>
        /// <returns></returns>
        public string ShowOpenDirectoryDialog(string title, string initialDirectory)
        {
            var dlg = new CommonOpenFileDialog
            {
                Title = title,
                IsFolderPicker = true,
                AddToMostRecentlyUsedList = false,
                AllowNonFileSystemItems = false,
                DefaultDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                Multiselect = false,
                ShowPlacesList = true
            };

            if (!string.IsNullOrWhiteSpace(initialDirectory))
                dlg.InitialDirectory = initialDirectory;

            return dlg.ShowDialog() == CommonFileDialogResult.Ok ? dlg.FileName : string.Empty;
        }

        /// <summary>
        /// Shows the save file dialog.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="initialDirectory">The initial directory.</param>
        /// <param name="extensionName">Name of the extension.</param>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public string ShowSaveFileDialog(string title, string initialDirectory, string extensionName, string extension)
        {
            var dlg = new CommonSaveFileDialog
            {
                Title = title,
                AddToMostRecentlyUsedList = false,
                DefaultDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                OverwritePrompt = true,
                ShowPlacesList = true,
                DefaultExtension = extension
            };
            dlg.Filters.Add(new CommonFileDialogFilter(extensionName, extension));

            return dlg.ShowDialog() == CommonFileDialogResult.Ok ? dlg.FileName : string.Empty;
        }

        /// <summary>
        /// Shows the open file dialog.
        /// </summary>
        /// <param name="title">The title.</param>
        /// <param name="initialDirectory">The initial directory.</param>
        /// <param name="extensionName">Name of the extension.</param>
        /// <param name="extension">The extension.</param>
        /// <returns></returns>
        public string ShowOpenFileDialog(string title, string initialDirectory, string extensionName, string extension)
        {
            var dlg = new CommonOpenFileDialog
            {
                Title = title,
                AddToMostRecentlyUsedList = false,
                DefaultDirectory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                EnsureFileExists = true,
                EnsurePathExists = true,
                EnsureReadOnly = false,
                EnsureValidNames = true,
                ShowPlacesList = true,
                DefaultExtension = extension
            };
            dlg.Filters.Add(new CommonFileDialogFilter(extensionName, extension));
            return dlg.ShowDialog() == CommonFileDialogResult.Ok ? dlg.FileName : string.Empty;
        }
    }
}