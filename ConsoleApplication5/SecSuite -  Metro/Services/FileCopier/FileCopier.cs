﻿using System.IO;

namespace Common.Services.FileCopier
{
    /// <summary>
    ///     FileCopier can copy directories and files with the same content as the existing files.
    /// </summary>
    internal class FileCopier
    {
        /// <summary>
        ///     Copies the directories to the output path. (No Files)
        /// </summary>
        /// <param name="sourceDirectory">The Source Directory Path</param>
        /// <param name="input">The input path.</param>
        /// <param name="output">The output path.</param>
        /// <returns></returns>
        public bool CopyDirectories(string sourceDirectory, string input, string output)
        {
            var toRemove = input.Length - sourceDirectory.Length;
            var toReturn = output + input.Remove(0, input.Length - toRemove);
            Directory.CreateDirectory(toReturn);
            return Directory.Exists(toReturn);
        }

        /// <summary>
        ///     Copies the files to the output path.
        /// </summary>
        /// <param name="sourceDirectory">The Source Directory Path</param>
        /// <param name="input">The input.</param>
        /// <param name="output">The output.</param>
        /// <returns></returns>
        public bool CopyFiles(string sourceDirectory, string input, string output)
        {
            var directoryName = Path.GetDirectoryName(input);
            if (directoryName == null) return false;
            var toRemove = input.Length - sourceDirectory.Length;
            var toReturn = output + input.Remove(0, input.Length - toRemove);
            File.Copy(input, toReturn, true);
            return File.Exists(toReturn);
        }
    }
}