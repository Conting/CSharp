﻿using System;
using System.Security.Cryptography;

namespace Common.Services.SecureNumberGenerator
{
    /// <summary>
    ///     SecureNumber provides pseudo-random number generation with properties that make it suitable for use in cryptography
    /// </summary>
    /// <seealso cref="System.Security.Cryptography.RandomNumberGenerator" />
    public class SecureNumber : RandomNumberGenerator
    {
        private readonly RandomNumberGenerator _rng = new RNGCryptoServiceProvider();


        /// <summary>
        ///     Generates the next pseudorandom number.
        /// </summary>
        /// <returns></returns>
        public int Next()
        {
            var data = new byte[sizeof(int)];
            _rng.GetBytes(data);
            return BitConverter.ToInt32(data, 0) & (int.MaxValue - 1);
        }

        /// <summary>
        ///     Generates the next pseudorandom number with a specified maximum value.
        /// </summary>
        /// <param name="maxValue">The maximum value.</param>
        /// <returns></returns>
        public int Next(int maxValue)
        {
            return Next(0, maxValue);
        }

        /// <summary>
        ///     Generates the next pseudorandom number with a specified maximum and minimum value.
        /// </summary>
        /// <param name="minValue">The minimum value.</param>
        /// <param name="maxValue">The maximum value.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public int Next(int minValue, int maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException();
            return (int) Math.Floor(minValue + ((double) maxValue - minValue)*NextDouble());
        }

        /// <summary>
        ///     Returns the next pseudorandom, uniformly distributed double value between 0.0 and 1.0 from this random number
        ///     generator's sequence.
        /// </summary>
        /// <returns></returns>
        public double NextDouble()
        {
            var data = new byte[sizeof(uint)];
            _rng.GetBytes(data);
            var randUint = BitConverter.ToUInt32(data, 0);
            return randUint/(uint.MaxValue + 1.0);
        }

        /// <summary>
        ///     When overridden in a derived class, fills an array of bytes with a cryptographically strong random sequence of
        ///     values.
        /// </summary>
        /// <param name="data">The array to fill with cryptographically strong random bytes.</param>
        public override void GetBytes(byte[] data)
        {
            _rng.GetBytes(data);
        }

        /// <summary>
        ///     When overridden in a derived class, fills an array of bytes with a cryptographically strong random sequence of
        ///     nonzero values.
        /// </summary>
        /// <param name="data">The array to fill with cryptographically strong random nonzero bytes.</param>
        public override void GetNonZeroBytes(byte[] data)
        {
            _rng.GetNonZeroBytes(data);
        }
    }
}