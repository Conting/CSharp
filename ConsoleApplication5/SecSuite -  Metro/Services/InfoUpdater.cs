﻿using System;
using Common.ViewModel;

namespace Common.Services
{
    /// <summary>
    /// Info Updater sets and get the title and status in the mainwindow.
    /// </summary>
    public sealed class InfoUpdater
    {
        private static readonly Lazy<InfoUpdater> Lazy =
            new Lazy<InfoUpdater>(() => new InfoUpdater());

        private InfoUpdater()
        {
        }

        /// <summary>
        /// Gets the InfoUpdater instance.
        /// </summary>
        /// <value>
        /// The singelton instance.
        /// </value>
        public static InfoUpdater Instance => Lazy.Value;

        /// <summary>
        /// Gets or sets the provider.
        /// </summary>
        /// <value>
        /// The provider.
        /// </value>
        public static WindowInfo Provider { get; set; }
    }
}