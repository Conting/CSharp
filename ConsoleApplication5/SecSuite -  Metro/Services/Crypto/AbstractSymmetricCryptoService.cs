﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using log4net;

namespace Common.Services.Crypto
{
    /// <summary>
    ///     AbstractSymmetricCryptoService provides methods for encrypting files with
    ///     the same cryptographic keys for both encryption of files and decryption of encrypted files.
    /// </summary>
    public class AbstractSymmetricCryptoService
    {
        private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        ///     Decrypts the inputFile with the password to the output path.
        /// </summary>
        /// <param name="inputFile">The input file.</param>
        /// <param name="password">The password.</param>
        /// <param name="output">The output.</param>
        public bool AES_Decrypt(string inputFile, string password, string output)
        {
            var passwordBytes = Encoding.UTF8.GetBytes(password);

            var hmacsha512 = new HMACSHA512(passwordBytes);
            var hash = hmacsha512.ComputeHash(passwordBytes);

            var salt = new byte[32];

            var toReadHash = new byte[hash.Length];

            var fsCrypt = new FileStream(inputFile, FileMode.Open);

            fsCrypt.Read(toReadHash, 0, hash.Length);
            if (hash.Where((t, i) => t != toReadHash[i]).Any())
            {
                _log.Error("Wrong Password at: " + inputFile);
                fsCrypt.Close();
                File.Delete(inputFile);
                return false;
            }


            fsCrypt.Read(salt, 0, salt.Length);

            var aes = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128
            };
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            aes.Key = key.GetBytes(aes.KeySize/8);
            aes.IV = key.GetBytes(aes.BlockSize/8);
            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.CFB;

            var cs = new CryptoStream(fsCrypt, aes.CreateDecryptor(), CryptoStreamMode.Read);
            var outputFile = Path.GetFileName(inputFile.Substring(0, inputFile.LastIndexOf('.')));
            var fsOut = new FileStream(output + @"\" + outputFile, FileMode.Create);

            var buffer = new byte[1048576];

            try
            {
                int read;
                while ((read = cs.Read(buffer, 0, buffer.Length)) > 0)
                    fsOut.Write(buffer, 0, read);
            }
            catch (CryptographicException ex)
            {
                _log.Error(ex.Message);
                _log.Error(ex.InnerException);
                fsOut.Close();
                fsCrypt.Close();
                File.Delete(output + outputFile);
                return false;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                _log.Error(ex.InnerException);
                fsCrypt.Close();
                fsOut.Close();
                File.Delete(output + outputFile);
                return false;
            }

            try
            {
                cs.Close();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                _log.Error(ex.InnerException);
            }
            finally
            {
                fsOut.Close();
                fsCrypt.Close();
            }
            return true;
        }

        /// <summary>
        ///     Generates a random salt.
        /// </summary>
        /// <returns></returns>
        public static byte[] GenerateRandomSalt()
        {
            var data = new byte[32];
            using (var rng = new RNGCryptoServiceProvider())
            {
                for (var i = 0; i < 10; i++)
                    rng.GetBytes(data);
            }
            return data;
        }

        /// <summary>
        ///     Encrypts the file from the inputFile-path with the password.
        /// </summary>
        /// <param name="inputFile">The input file.</param>
        /// <param name="password">The password.</param>
        /// <param name="extension"></param>
        public bool AES_Encrypt(string inputFile, string password, string extension)
        {
            //generate random salt
            var salt = GenerateRandomSalt();

            //create output file name
            var fsCrypt = new FileStream(inputFile + extension, FileMode.Create);

            //convert password string to byte arrray
            var passwordBytes = Encoding.UTF8.GetBytes(password);

            //Set symmetric encryption algorithm
            var aes = new RijndaelManaged
            {
                KeySize = 256,
                BlockSize = 128,
                Padding = PaddingMode.PKCS7
            };

            //set key and iv bytes
            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            aes.Key = key.GetBytes(aes.KeySize/8);
            aes.IV = key.GetBytes(aes.BlockSize/8);

            aes.Mode = CipherMode.CFB;

            var hmacsha512 = new HMACSHA512(passwordBytes);
            var hash = hmacsha512.ComputeHash(passwordBytes);

            //write hash to the begining of the output file, so in this case can be random every time
            fsCrypt.Write(hash, 0, hash.Length);

            //write salt it case can be random every time
            fsCrypt.Write(salt, 0, salt.Length);

            var cs = new CryptoStream(fsCrypt, aes.CreateEncryptor(), CryptoStreamMode.Write);

            var fsIn = new FileStream(inputFile, FileMode.Open);

            //1mb file buffer
            var buffer = new byte[1048576];

            try
            {
                int read;
                while ((read = fsIn.Read(buffer, 0, buffer.Length)) > 0)
                    cs.Write(buffer, 0, read);


                fsIn.Close();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                _log.Error(ex.InnerException);
                return false;
            }
            finally
            {
                cs.Close();
                fsCrypt.Close();
            }
            return true;
        }
    }
}