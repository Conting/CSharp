﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Common.Converter
{
    /// <summary>
    ///     Provides methods for converting Lists into a OberserableCollection and back.
    /// </summary>
    internal class ListConverter
    {
        /// <summary>
        ///     Converts the list to observable collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toConvert">To convert.</param>
        /// <param name="toReturn">To return.</param>
        /// <returns></returns>
        public ObservableCollection<T> ConvertListToObservable<T>(List<T> toConvert, ObservableCollection<T> toReturn)
        {
            foreach (var content in toConvert)
                toReturn.Add(content);
            return toReturn;
        }

        /// <summary>
        ///     Converts the observable collection to list.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="toConvert">To convert.</param>
        /// <param name="toReturn">To return.</param>
        /// <returns></returns>
        public List<T> ConvertObservableToList<T>(ObservableCollection<T> toConvert, List<T> toReturn)
        {
            toReturn.AddRange(toConvert);
            return toReturn;
        }
    }
}