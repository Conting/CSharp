﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Forms;
using Common.Commons;
using Common.Converter;
using Common.Helpers;
using Common.Services.Commands;
using Common.Services.Pwgenerator;

namespace Common.ViewModel
{
    /// <summary>
    ///     The PwGeneratorViewModel's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class PwGeneratorViewModel : ViewModelBase
    {
        private readonly ListConverter _listConverter = new ListConverter();

        private IDialogService _dialogService;
        private List<string> _generatedPasswords;
        private bool _isPasswordFlyoutOpen;
        private bool _lCase;
        private bool _numeric;
        private string _password;
        private PwGenerator _pwGenerator;
        private int _pwLength;
        private bool _special;
        private ObservableCollection<string> _toSet;
        private bool _uCase;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PwGeneratorViewModel" /> class.
        /// </summary>
        /// <param name="dialogService">The dialog service.</param>
        public PwGeneratorViewModel(IDialogService dialogService)
        {
            DialogService = dialogService;
            Initialize();
        }

        public static PwGeneratorViewModel Instance { get; set; }

        /// <summary>
        ///     Gets or sets the IncreaseValueCommand command.
        /// </summary>
        /// <value>
        ///     The IncreaseValueCommand command.
        /// </value>
        public RelayCommand IncreaseValueCommand { get; set; }

        /// <summary>
        ///     Gets or sets the DecreaseValueCommand command.
        /// </summary>
        /// <value>
        ///     The DecreaseValueCommand command.
        /// </value>
        public RelayCommand DecreaseValueCommand { get; set; }

        /// <summary>
        ///     Gets or sets the GeneratePasswordCommand command.
        /// </summary>
        /// <value>
        ///     The GeneratePasswordCommand command.
        /// </value>
        public RelayCommand GeneratePasswordCommand { get; set; }

        /// <summary>
        ///     Gets or sets the CopyStringToClipboard command.
        /// </summary>
        /// <value>
        ///     The CopyStringToClipboardCommand command.
        /// </value>
        public RelayCommand CopyStringToClipboardCommand { get; set; }

        /// <summary>
        ///     Gets or sets the password to set.
        /// </summary>
        /// <value>
        ///     The password to set.
        /// </value>
        public string PasswordToSet
        {
            get { return _password; }
            set
            {
                if (value.Equals(_password)) return;
                _password = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the dialog service.
        /// </summary>
        /// <value>
        ///     The dialog service.
        /// </value>
        public IDialogService DialogService
        {
            get { return _dialogService; }
            set
            {
                if (value.Equals(_dialogService)) return;
                _dialogService = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance's password flyout is open.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance password flyout is open; otherwise, <c>false</c>.
        /// </value>
        public bool IsPasswordFlyoutOpen
        {
            get { return _isPasswordFlyoutOpen; }
            set
            {
                if (value.Equals(_isPasswordFlyoutOpen)) return;
                _isPasswordFlyoutOpen = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the length of the password.
        /// </summary>
        /// <value>
        ///     The length of the password.
        /// </value>
        public int PwLength
        {
            get { return _pwLength; }
            set
            {
                if (value.Equals(_pwLength)) return;
                _pwLength = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether password will be generated with special letters.
        /// </summary>
        /// <value>
        ///     <c>true</c> if _special equals value; otherwise, <c>false</c>.
        /// </value>
        public bool Special
        {
            get { return _special; }
            set
            {
                if (value.Equals(_special)) return;
                _special = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the password will be generated  with numeric letters.
        /// </summary>
        /// <value>
        ///     <c>true</c> if _numeric equals value; otherwise, <c>false</c>.
        /// </value>
        public bool Numeric
        {
            get { return _numeric; }
            set
            {
                if (value.Equals(_numeric)) return;
                _numeric = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the password will be generated with upper letters .
        /// </summary>
        /// <value>
        ///     <c>true</c> if _uCase equals value; otherwise, <c>false</c>.
        /// </value>
        public bool UCase
        {
            get { return _uCase; }
            set
            {
                if (value.Equals(_uCase)) return;
                _uCase = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether the password will be generated with lower letters .
        /// </summary>
        /// <value>
        ///     <c>true</c> if _lcase equals value; otherwise, <c>false</c>.
        /// </value>
        public bool LCase
        {
            get { return _lCase; }
            set
            {
                if (value.Equals(_lCase)) return;
                _lCase = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets ObserableCollection list to display in view.
        /// </summary>
        /// <value>
        ///     The ObserableCollection
        /// </value>
        public ObservableCollection<string> ToSet
        {
            get { return _toSet; }
            set
            {
                if (value.Equals(_toSet)) return;
                _toSet = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the generated passwords list.
        /// </summary>
        /// <value>
        ///     The generated passwords list.
        /// </value>
        public List<string> GeneratedPasswords
        {
            get { return _generatedPasswords; }
            set
            {
                if (value.Equals(_generatedPasswords)) return;
                _generatedPasswords = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Initializes method this instance.
        /// </summary>
        public void Initialize()
        {
            PwLength = 16;
            PasswordToSet = "Init";
            UCase = true;
            LCase = true;
            Numeric = true;
            _pwGenerator = new PwGenerator();
            GeneratedPasswords = new List<string>();
            ToSet = new ObservableCollection<string>();
            IncreaseValueCommand = new RelayCommand(_ => OnIncreaseValueCommand());
            DecreaseValueCommand = new RelayCommand(_ => OnDecreaseValueCommand());
            GeneratePasswordCommand = new RelayCommand(_ => OnGeneratePasswords());
            CopyStringToClipboardCommand = new RelayCommand(_ => OnCopyPasswordToClipboardCommand());
            Instance = this;
        }

        /// <summary>
        ///     Called when DecreaseValueCommand RelayCommand is called.
        /// </summary>
        private void OnDecreaseValueCommand()
        {
            DecreaseValue();
        }

        /// <summary>
        ///     Decreases PwLength value.
        /// </summary>
        private void DecreaseValue()
        {
            PwLength = PwLength - 1;
            if (PwLength < 0) PwLength = PwLength + 1;
        }

        /// <summary>
        ///     Called when IncreaseValueCommand RelayCommand is called.
        /// </summary>
        private void OnIncreaseValueCommand()
        {
            IncreaseValue();
        }

        /// <summary>
        ///     Increases PwLength value.
        /// </summary>
        private void IncreaseValue()
        {
            PwLength = PwLength + 1;
            if (PwLength > 150) PwLength = PwLength - 1;
        }

        /// <summary>
        ///     Called when GeneratePasswordCommand RelayCommand is called,
        ///     which generates password and places them in the flyout's datagrid.
        /// </summary>
        private async void OnGeneratePasswords()
        {
            if (await Task.Run(() => GeneratePasswords().Result.Equals(false))) return;
            IsPasswordFlyoutOpen = true;
        }

        /// <summary>
        ///     Called when CopyStringToClipboardCommand RelayCommand is called, which copies the password to the clipboard.
        /// </summary>
        private void OnCopyPasswordToClipboardCommand()
        {
            if (string.IsNullOrWhiteSpace(PasswordToSet)) return;
            Clipboard.SetText(PasswordToSet);
            PasswordToSet = "Init";
            IsPasswordFlyoutOpen = false;
        }

        /// <summary>
        ///     Generates passwords for generated passwords list.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> GeneratePasswords()
        {
            if ((LCase == false) && (UCase == false) && (Numeric == false) && (Special == false))
            {
                await DialogService.ShowMessageAsync("Cannot generate Passwords!", "Please tick at least one option!");
                return false;
            }

            var initList = new List<string>();
            if (LCase)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_LCASE);
            if (UCase)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_UCASE);
            if (Numeric)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_NUMERIC);
            if (Special)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_SPECIAL);
            GeneratedPasswords.Clear();
            for (var i = 0; i < 100; ++i)
                GeneratedPasswords.Add(_pwGenerator.GeneratePassword(_pwLength, initList));
            ToSet = _listConverter.ConvertListToObservable(GeneratedPasswords, new ObservableCollection<string>());
            return true;
        }
    }
}