﻿using System;
using System.Threading.Tasks;
using Common.Helpers;
using Common.Model;
using Common.Services;
using Common.Services.Commands;
using MahApps.Metro.Controls.Dialogs;

namespace Common.ViewModel
{
    /// <summary>
    ///     The MainWindowViewModel's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IDialogService _dialogService;
        private CreateLoginViewModel _createLoginViewModel;
        private DatabaseViewModel _databaseViewModel;
        private DecryptionViewModel _decryptionViewModel;
        private User _dummyUser;
        private EditLoginViewModel _editLoginViewModel;
        private EncryptionViewModel _encryptionViewModel;
        private bool _isSettingsFlyoutOpen;
        private PasswordTesterViewModel _passwordTesterViewModel;
        private PwGeneratorViewModel _pwGeneratorViewModel;
        private SettingsViewModel _settingsViewModel;
        private WindowInfo _windowInfo;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainWindowViewModel" /> class.
        /// </summary>
        /// <param name="dialogService">The dialog service.</param>
        public MainWindowViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;
            DatabaseViewModel = new DatabaseViewModel(dialogService);

            Initializate();
        }

        /// <summary>
        ///     Gets or sets the window information.
        /// </summary>
        /// <value>
        ///     The window information.
        /// </value>
        public WindowInfo WindowInfo
        {
            get { return _windowInfo; }
            set
            {
                if (value.Equals(_windowInfo)) return;
                _windowInfo = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the ApplicationExitCommand, which closes the application.
        /// </summary>
        /// <value>
        ///     The application exit command.
        /// </value>
        public RelayCommand ApplicationExitCommand { get; set; }

        /// <summary>
        ///     Gets or sets the Settings RelayCommand, which opens the Setting Flyout.
        /// </summary>
        /// <value>
        ///     The settings command.
        /// </value>
        public RelayCommand Settings { get; set; }

        /// <summary>
        ///     Gets the CtrlS RelayCommand, which saves the Database.
        /// </summary>
        /// <value>
        ///     The CtrlS command.
        /// </value>
        public RelayCommand CtrlS { get; private set; }

        /// <summary>
        ///     Gets or sets the PasswordTesterViewModel.
        /// </summary>
        /// <value>
        ///     The PasswordTesterViewModel.
        /// </value>
        public PasswordTesterViewModel PasswordTesterViewModel
        {
            get { return _passwordTesterViewModel; }
            set
            {
                if (value.Equals(_passwordTesterViewModel)) return;
                _passwordTesterViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the DecryptionViewModel.
        /// </summary>
        /// <value>
        ///     The DecryptionViewModel.
        /// </value>
        public DecryptionViewModel DecryptionViewModel
        {
            get { return _decryptionViewModel; }
            set
            {
                if (value.Equals(_decryptionViewModel)) return;
                _decryptionViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the EncryptionViewModel.
        /// </summary>
        /// <value>
        ///     The EncryptionViewModel.
        /// </value>
        public EncryptionViewModel EncryptionViewModel
        {
            get { return _encryptionViewModel; }
            set
            {
                if (value.Equals(_encryptionViewModel)) return;
                _encryptionViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the PwGeneratorViewModel.
        /// </summary>
        /// <value>
        ///     The PwGeneratorViewModel.
        /// </value>
        public PwGeneratorViewModel PwGeneratorViewModel
        {
            get { return _pwGeneratorViewModel; }
            set
            {
                if (value.Equals(_pwGeneratorViewModel)) return;
                _pwGeneratorViewModel = value;
                OnPropertyChanged();
            }
        }


        /// <
        ///     summary>
        ///     Gets or sets the dummy user, which is needed so EditLoginViewModel is not null at initializing.
        /// </summary>
        /// <value>
        ///     The dummy user object.
        /// </value>
        public User DummyUser
        {
            get { return _dummyUser; }
            set
            {
                if (value.Equals(_dummyUser)) return;
                _dummyUser = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        ///     Gets or sets the SettingsViewModel.
        /// </summary>
        /// <value>
        ///     The SettingsViewModel.
        /// </value>
        public SettingsViewModel SettingsViewModel
        {
            get { return _settingsViewModel; }
            set
            {
                if (value.Equals(_settingsViewModel)) return;
                _settingsViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the EditLoginViewModel.
        /// </summary>
        /// <value>
        ///     The EditLoginViewModel.
        /// </value>
        public EditLoginViewModel EditLoginViewModel
        {
            get { return _editLoginViewModel; }
            set
            {
                if (value.Equals(_editLoginViewModel)) return;
                _editLoginViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the CreateLoginViewModel.
        /// </summary>
        /// <value>
        ///     The CreateLoginViewModel.
        /// </value>
        public CreateLoginViewModel CreateLoginViewModel
        {
            get { return _createLoginViewModel; }
            set
            {
                if (value.Equals(_createLoginViewModel)) return;
                _createLoginViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the DatabaseViewModel.
        /// </summary>
        /// <value>
        ///     The DatabaseViewModel.
        /// </value>
        public DatabaseViewModel DatabaseViewModel
        {
            get { return _databaseViewModel; }
            set
            {
                if (value.Equals(_databaseViewModel)) return;
                _databaseViewModel = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        ///     Gets or sets a value indicating whether this instance's settings flyout is open.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance's settings flyout is open; otherwise, <c>false</c>.
        /// </value>
        public bool IsSettingsFlyoutOpen
        {
            get { return _isSettingsFlyoutOpen; }
            set
            {
                if (value.Equals(_isSettingsFlyoutOpen)) return;
                _isSettingsFlyoutOpen = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Initialization method for instance.
        /// </summary>
        private void Initializate()
        {
            var stringUpdater = InfoUpdater.Instance;
            SettingsViewModel = new SettingsViewModel(() => IsSettingsFlyoutOpen = false);
            Settings = new RelayCommand(_ => OnSettingsCommand());
            CtrlS = new RelayCommand(_ => SaveCommand());
            PwGeneratorViewModel = new PwGeneratorViewModel(_dialogService);
            EncryptionViewModel = new EncryptionViewModel(_dialogService);
            DecryptionViewModel = new DecryptionViewModel(_dialogService);
            PasswordTesterViewModel = new PasswordTesterViewModel(_dialogService);
            ApplicationExitCommand = new RelayCommand(_ => OnApplicationExitCommand());
            var init = new WindowInfo
            {
                Status = "init",
                Title = "SecSuite",
                Changes = false
            };
            WindowInfo = init;
            InfoUpdater.Provider = init;
        }

        /// <summary>
        ///     Called when application is closing.
        /// </summary>
        private async void OnApplicationExitCommand()
        {
            if (InfoUpdater.Provider.Changes)
                if (await Task.Run(
                    () =>
                        _dialogService.AskQuestionAsync("Warning",
                                "There are unsaved changes! Do you want to save your database before closing the application?")
                            .Result ==
                        MessageDialogResult.Affirmative))
                    DatabaseViewModel.SaveDatabase();

            if (
                await Task.Run(
                    () =>
                        _dialogService.AskQuestionAsync("Closing Application",
                            "Are your sure that you want to close this application?").Result ==
                        MessageDialogResult.Affirmative))
                Environment.Exit(0);
        }

        /// <summary>
        ///     Called when Settings RelayCommand is called, when setting button is clicked on.
        /// </summary>
        private void OnSettingsCommand()
        {
            IsSettingsFlyoutOpen = true;
        }


        /// <summary>
        ///     Saves the Database when Ctrl+S is pressed.
        /// </summary>
        private void SaveCommand()
        {
            DatabaseViewModel.SaveDatabase();
        }
    }
}