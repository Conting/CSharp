﻿using System;

namespace Common.ViewModel
{
    /// <summary>
    ///     The SettingView's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class SettingsViewModel : ViewModelBase
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="SettingsViewModel" /> class.
        /// </summary>
        /// <param name="closeAction">The close action.</param>
        public SettingsViewModel(Action closeAction)
        {
        }
    }
}