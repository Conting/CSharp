﻿namespace Common.ViewModel
{
    /// <summary>
    ///     This class provides Title and Status information.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class WindowInfo : ViewModelBase
    {
        private bool _changes;
        private string _status;
        private string _title;

        /// <summary>
        ///     Gets or sets the status.
        /// </summary>
        /// <value>
        ///     The status.
        /// </value>
        public string Status
        {
            get { return _status; }
            set
            {
                if (value.Equals(_status)) return;
                _status = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        /// <value>
        ///     The title.
        /// </value>
        public string Title
        {
            get { return _title; }
            set
            {
                if (value.Equals(_title)) return;
                _title = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether there a changed made to the database.
        /// </summary>
        /// <value>
        ///     <c>true</c> if changes were made; otherwise, <c>false</c>.
        /// </value>
        public bool Changes
        {
            get { return _changes; }
            set
            {
                if (value.Equals(_changes)) return;
                _changes = value;
                OnPropertyChanged();
            }
        }
    }
}