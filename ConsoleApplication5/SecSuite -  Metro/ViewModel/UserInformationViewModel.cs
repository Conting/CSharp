﻿using System;
using System.Diagnostics;
using System.Windows;
using Common.Helpers;
using Common.Model;

namespace Common.ViewModel
{
    /// <summary>
    ///     The UserInformationView's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class UserInformationViewModel : ViewModelBase
    {
        private string _password;
        private string _title;
        private string _username;
        private string _website;

        /// <summary>
        ///     Initializes a new instance of the <see cref="UserInformationViewModel" /> class.
        /// </summary>
        /// <param name="closeAction">Flyout close action.</param>
        /// <param name="user">User object.</param>
        public UserInformationViewModel(Action closeAction, User user)
        {
            CopyPassword = new RelayCommand(_ => OnCopyPassword());
            CopyTitle = new RelayCommand(_ => OnCopyTitle());
            CopyUsername = new RelayCommand(_ => OnCopyUsername());
            VisitWebsite = new RelayCommand(_ => OnVisitWebsite());
        }

        /// <summary>
        ///     Gets or sets the CopyTitle command.
        /// </summary>
        /// <value>
        ///     The title CopyTitle command.
        /// </value>
        public RelayCommand CopyTitle { get; set; }

        /// <summary>
        ///     Gets or sets CopyPassword command.
        /// </summary>
        /// <value>
        ///     The CopyPassword command.
        /// </value>
        public RelayCommand CopyPassword { get; set; }

        /// <summary>
        ///     Gets or sets the CopyUsername command.
        /// </summary>
        /// <value>
        ///     The CopyUsername command.
        /// </value>
        public RelayCommand CopyUsername { get; set; }

        /// <summary>
        ///     Gets or sets the VisitWebsite command.
        /// </summary>
        /// <value>
        ///     The VisitWebsite command.
        /// </value>
        public RelayCommand VisitWebsite { get; set; }

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        /// <value>
        ///     The title.
        /// </value>
        public string Title
        {
            get { return _title; }
            set
            {
                if (value.Equals(_title)) return;
                _title = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the username.
        /// </summary>
        /// <value>
        ///     The username.
        /// </value>
        public string Username
        {
            get { return _username; }
            set
            {
                if (value.Equals(_username)) return;
                _username = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the password.
        /// </summary>
        /// <value>
        ///     The password.
        /// </value>
        public string Password
        {
            get { return _password; }
            set
            {
                if (value.Equals(_password)) return;
                _password = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the website.
        /// </summary>
        /// <value>
        ///     The website.
        /// </value>
        public string Website
        {
            get { return _website; }
            set
            {
                if (value.Equals(_website)) return;
                _website = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Called when VisitWebsite RelayCommand is called, which open the website in a webbrowser.
        /// </summary>
        private void OnVisitWebsite()
        {
            if (Website.Contains("http://") || Website.Contains("https://"))
                Process.Start(Website);
            else
                Process.Start("http://" + Website);
        }

        /// <summary>
        ///     Called when CopyUsername RelayCommand is called, which copies the Username in clipboard.
        /// </summary>
        private void OnCopyUsername()
        {
            Clipboard.SetText(Username);
        }

        /// <summary>
        ///     Called when CopyTitle RelayCommand is called, which copies the Title in clipboard.
        /// </summary>
        private void OnCopyTitle()
        {
            Clipboard.SetText(Title);
        }

        /// <summary>
        ///     Called when CopyPassword RelayCommand is called, which copies the Password in clipboard.
        /// </summary>
        private void OnCopyPassword()
        {
            Clipboard.SetText(Password);
        }

        /// <summary>
        ///     Sets the user information.
        /// </summary>
        /// <param name="user">The user object.</param>
        /// <returns></returns>
        public bool SetInformation(User user)
        {
            if (user.Equals(null)) return false;
            Title = user.Title;
            Password = user.Password;
            Username = user.Username;
            Website = user.Website;
            return true;
        }

        /// <summary>
        ///     Clears this instance's textboxes.
        /// </summary>
        public void Clear()
        {
            Title = "";
            Password = "";
            Username = "";
            Website = "";
        }
    }
}