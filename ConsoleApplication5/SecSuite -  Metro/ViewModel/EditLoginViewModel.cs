﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Commons;
using Common.Helpers;
using Common.Model;
using Common.Services;
using Common.Services.Commands;
using Common.Services.Pwgenerator;

namespace Common.ViewModel
{
    /// <summary>
    ///     The EditLoginViewModel's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class EditLoginViewModel : ViewModelBase
    {
        private readonly PwGenerator _pwGenerator = new PwGenerator();
        private DatabaseViewModel _databaseViewModel;
        private IDialogService _dialogService;
        private string _password;
        private int _selectedRowIndex;

        private string _title;
        private User _user;
        private string _username;
        private string _website;

        /// <summary>
        ///     Initializes a new instance of the <see cref="EditLoginViewModel" /> class.
        /// </summary>
        /// <param name="closeAction">The close action.</param>
        /// <param name="user">User Object.</param>
        /// <param name="databaseViewModel">The database view model.</param>
        /// <param name="selectedRowIndex">Index of the selected row.</param>
        /// <param name="dialogService">The dialog service.</param>
        public EditLoginViewModel(Action closeAction, User user, DatabaseViewModel databaseViewModel,
            int selectedRowIndex, IDialogService dialogService)
        {
            User = user;
            SelectedRowIndex = selectedRowIndex;
            Title = user.Title;
            Username = user.Username;
            Password = user.Password;
            Website = user.Website;
            GeneratePasswordCommand = new RelayCommand(_ => OnGeneratePassword());
            EditUserCommand = new RelayCommand(_ => OnEditUserCommand());
            DataBaseViewModel = databaseViewModel;
            DialogService = dialogService;
        }


        /// <summary>
        ///     Gets or sets the GeneratePasswordCommand command.
        /// </summary>
        /// <value>
        ///     The GeneratePasswordCommand.
        /// </value>
        public RelayCommand GeneratePasswordCommand { get; set; }

        /// <summary>
        ///     Gets or sets the EditUserCommand command.
        /// </summary>
        /// <value>
        ///     The EditUserCommand.
        /// </value>
        public RelayCommand EditUserCommand { get; set; }

        /// <summary>
        ///     Gets or sets the dialog service.
        /// </summary>
        /// <value>
        ///     The dialog service.
        /// </value>
        public IDialogService DialogService
        {
            get { return _dialogService; }
            set
            {
                if (value.Equals(_dialogService)) return;
                _dialogService = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the DatabaseViewModel.
        /// </summary>
        /// <value>
        ///     The DatabaseViewModel.
        /// </value>
        public DatabaseViewModel DataBaseViewModel
        {
            get { return _databaseViewModel; }
            set
            {
                if (value.Equals(_databaseViewModel)) return;
                _databaseViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the index of the selected row.
        /// </summary>
        /// <value>
        ///     The index of the selected row.
        /// </value>
        public int SelectedRowIndex
        {
            get { return _selectedRowIndex; }
            set
            {
                if (value.Equals(_selectedRowIndex)) return;
                _selectedRowIndex = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the user to edit.
        /// </summary>
        /// <value>
        ///     The user object.
        /// </value>
        public User User
        {
            get { return _user; }
            set
            {
                if (value.Equals(_user)) return;
                _user = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the title to edit.
        /// </summary>
        /// <value>
        ///     The title.
        /// </value>
        public string Title
        {
            get { return _title; }
            set
            {
                if (value.Equals(_title)) return;
                _title = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the username to edit.
        /// </summary>
        /// <value>
        ///     The username.
        /// </value>
        public string Username
        {
            get { return _username; }
            set
            {
                if (value.Equals(_username)) return;
                _username = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the password to edit.
        /// </summary>
        /// <value>
        ///     The password.
        /// </value>
        public string Password
        {
            get { return _password; }
            set
            {
                if (value.Equals(_password)) return;
                _password = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the website to edit.
        /// </summary>
        /// <value>
        ///     The website.
        /// </value>
        public string Website
        {
            get { return _website; }
            set
            {
                if (value.Equals(_website)) return;
                _website = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Called when EditUserCommand RelayCommand is called, #
        ///     which validates user information and replaces user in database.
        /// </summary>
        private async void OnEditUserCommand()
        {
            if (!await Task.Run(() => Validation().Result.Equals(true))) return;
            User.Title = Title;
            User.Password = Password;
            User.Modified = DateTime.Now;
            User.Username = Username;
            DataBaseViewModel.ReplaceUser(User, SelectedRowIndex);
            DataBaseViewModel.IsEditLoginInfoFlyoutOpen = false;
            InfoUpdater.Provider.Changes = true;
            InfoUpdater.Provider.Title = InfoUpdater.Provider.Title + "*";
            Clear();
        }

        /// <summary>
        ///     Called when GeneratePasswordCommand RelayCommand is called, which generates
        ///     a 16 character long password and sets it to the password property.
        /// </summary>
        private void OnGeneratePassword()
        {
            var pwGeneratorInstance = PwGeneratorViewModel.Instance;
            var initList = new List<string>();
            if (pwGeneratorInstance.LCase)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_LCASE);
            if (pwGeneratorInstance.UCase)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_UCASE);
            if (pwGeneratorInstance.Numeric)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_NUMERIC);
            if (pwGeneratorInstance.Special)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_SPECIAL);
            var toSet = _pwGenerator.GeneratePassword(pwGeneratorInstance.PwLength, initList);
            Password = toSet;
        }

        /// <summary>
        ///     Validations this instance's textboxes.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> Validation()
        {
            if (string.IsNullOrWhiteSpace(Title))
            {
                await DialogService.ShowMessageAsync("Title cannot be empty!", "Please fill textbox out!");
                return false;
            }
            if (string.IsNullOrWhiteSpace(Username))
            {
                await DialogService.ShowMessageAsync("Username cannot be empty!", "Please fill textbox out!");

                return false;
            }
            if (string.IsNullOrWhiteSpace(Password))
            {
                await DialogService.ShowMessageAsync("Password cannot be empty!", "Please fill passwordbox out!");

                return false;
            }
            if (string.IsNullOrWhiteSpace(Website))
            {
                await DialogService.ShowMessageAsync("Website cannot be empty!", "Please fill textbox out!");

                return false;
            }
            return true;
        }

        /// <summary>
        ///     Clears this instance's textboxes.
        /// </summary>
        public void Clear()
        {
            Title = "";
            Password = "";
            Username = "";
            Website = "";
        }
    }
}