﻿using System.Text.RegularExpressions;
using Common.Helpers;
using Common.Services.Commands;

namespace Common.ViewModel
{
    /// <summary>
    ///     The PasswordTesterView's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class PasswordTesterViewModel : ViewModelBase
    {
        private int _barValue;
        private bool _long;
        private bool _luCase;
        private bool _numeric;
        private string _password;
        private int _score;
        private bool _special;

        /// <summary>
        ///     Initializes a new instance of the <see cref="PasswordTesterViewModel" /> class.
        /// </summary>
        /// <param name="dialogService">The dialog service.</param>
        public PasswordTesterViewModel(IDialogService dialogService)
        {
            PasswordTestCommand = new RelayCommand(_ => OnPasswordTestCommand());
            Score = 0;
        }

        /// <summary>
        ///     Gets or sets the PasswordTestCommand command.
        /// </summary>
        /// <value>
        ///     The PasswordTestCommand command.
        /// </value>
        public RelayCommand PasswordTestCommand { get; set; }

        /// <summary>
        ///     Gets or sets the password strength score.
        /// </summary>
        /// <value>
        ///     The score.
        /// </value>
        public int Score
        {
            get { return _score; }
            set
            {
                if (value.Equals(_score)) return;
                _score = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the password to test.
        /// </summary>
        /// <value>
        ///     The password.
        /// </value>
        public string Password
        {
            get { return _password; }
            set
            {
                if (value.Equals(_password)) return;
                _password = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the bar value to display.
        /// </summary>
        /// <value>
        ///     The bar value.
        /// </value>
        public int BarValue
        {
            get { return _barValue; }
            set
            {
                if (value.Equals(_barValue)) return;
                _barValue = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether password was generated with special letters.
        /// </summary>
        /// <value>
        ///     <c>true</c> if _special equals value; otherwise, <c>false</c>.
        /// </value>
        public bool Special
        {
            get { return _special; }
            set
            {
                if (value.Equals(_special)) return;
                _special = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether password was generated with enough characters.
        /// </summary>
        /// <value>
        ///     <c>true</c> if _long equals value; otherwise, <c>false</c>.
        /// </value>
        public bool Long
        {
            get { return _long; }
            set
            {
                if (value.Equals(_long)) return;
                _long = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether password was generated with numerical letters.
        /// </summary>
        /// <value>
        ///     <c>true</c> if _numerical equals value; otherwise, <c>false</c>.
        /// </value>
        public bool Numeric
        {
            get { return _numeric; }
            set
            {
                if (value.Equals(_numeric)) return;
                _numeric = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether password was generated with upper & lower letters.
        /// </summary>
        /// <value>
        ///     <c>true</c> if _luCase equals value; otherwise, <c>false</c>.
        /// </value>
        public bool LuCase
        {
            get { return _luCase; }
            set
            {
                if (value.Equals(_luCase)) return;
                _luCase = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Called when PasswordTestCommand RelayCommand is called,
        ///     which test the password strength and sets the bar value.
        /// </summary>
        private void OnPasswordTestCommand()
        {
            BarValue = CheckStrength(Password);
        }

        /// <summary>
        ///     Checks the password strength.
        /// </summary>
        /// <param name="password">The password.</param>
        /// <returns></returns>
        public int CheckStrength(string password)
        {
            LuCase = false;
            Numeric = false;
            Long = false;
            Special = false;
            Score = 0;
            if (string.IsNullOrWhiteSpace(password))
                return 0;
            if (password.Length < 4)
                return 0;
            if (password.Length >= 8)
                Score++;
            if (password.Length >= 16)
            {
                Score++;
                Long = true;
            }
            if (Regex.IsMatch(password, @"[0-9]+(\.[0-9][0-9]?)?", RegexOptions.ECMAScript))
            {
                Score++;
                Numeric = true;
            }
            if (Regex.IsMatch(password, @"^(?=.*[a-z])(?=.*[A-Z]).+$", RegexOptions.ECMAScript))
            {
                Score++;
                LuCase = true;
            }
            if (!Regex.IsMatch(password, @"[!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]", RegexOptions.ECMAScript)) return Score;
            Score++;
            Special = true;

            return Score;
        }
    }
}