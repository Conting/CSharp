﻿using System.IO;
using System.Threading.Tasks;
using Common.Commons;
using Common.Core;
using Common.Helpers;
using Common.Services.Commands;
using MahApps.Metro.Controls.Dialogs;

namespace Common.ViewModel
{
    /// <summary>
    ///     The DecryptionViewModel's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class DecryptionViewModel : ViewModelBase
    {
        private readonly IDialogService _dialogService;
        private string _destinationPath;

        private EncryptDecryptIo _encryptDecryptIo;
        private bool _isDecryptionButtonEnabled;
        private string _password;
        private string _sourcePath;

        /// <summary>
        ///     Initializes a new instance of the <see cref="DecryptionViewModel" /> class.
        /// </summary>
        /// <param name="dialogService">The dialog service.</param>
        public DecryptionViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;
            Initialize();
        }

        /// <summary>
        ///     Gets or sets the SourcePathCommand command.
        /// </summary>
        /// <value>
        ///     The SourcePathCommand.
        /// </value>
        public RelayCommand SourcePathCommand { get; set; }

        /// <summary>
        ///     Gets or sets the DestPathCommand command.
        /// </summary>
        /// <value>
        ///     The DestPathCommand.
        /// </value>
        public RelayCommand DestPathCommand { get; set; }

        /// <summary>
        ///     Gets or sets the DecryptCommand command.
        /// </summary>
        /// <value>
        ///     The DecryptCommand.
        /// </value>
        public RelayCommand DecryptCommand { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance's decryption button is enabled.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance decryption button is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsDecryptionButtonEnabled
        {
            get { return _isDecryptionButtonEnabled; }
            set
            {
                if (value.Equals(_isDecryptionButtonEnabled)) return;
                _isDecryptionButtonEnabled = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        ///     Gets or sets the encryption password.
        /// </summary>
        /// <value>
        ///     The password.
        /// </value>
        public string Password
        {
            get { return _password; }
            set
            {
                if (value.Equals(_password)) return;
                _password = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the source path of the folder to decrypt .
        /// </summary>
        /// <value>
        ///     The source path.
        /// </value>
        public string SourcePath
        {
            get { return _sourcePath; }
            set
            {
                if (value.Equals(_sourcePath)) return;
                _sourcePath = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the destination path, where the encrypted files are saved.
        /// </summary>
        /// <value>
        ///     The destination path.
        /// </value>
        public string DestinationPath
        {
            get { return _destinationPath; }
            set
            {
                if (value.Equals(_destinationPath)) return;
                _destinationPath = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Initialization method for this instance.
        /// </summary>
        private void Initialize()
        {
            DestPathCommand = new RelayCommand(_ => OnDestPathCommand());
            SourcePathCommand = new RelayCommand(_ => OnSourcePathCommand());
            DecryptCommand = new RelayCommand(_ => OnDecryptCommand());
            _encryptDecryptIo = new EncryptDecryptIo();
        }

        /// <summary>
        ///     Called when DecryptCommand RelayCommand is called, which starts the decryption process.
        /// </summary>
        private async void OnDecryptCommand()
        {
            if (await Task.Run(() => Validation().Result.Equals(false))) return;
            IsDecryptionButtonEnabled = false;
            if (!await Task.Run(() => _dialogService.AskQuestionAsync(Constants.WARNINGS.DECRYPTION_WARNING,
                                              Constants.QUESTIONS.DECRPYTION_QUESTION +
                                              "\n\n Your Data will be decrytped with a AES 256 Bit Key, PKCS7 Padding in CFB mode!")
                                          .Result ==
                                      MessageDialogResult.Affirmative)) return;
            IsDecryptionButtonEnabled = false;
            if (await Task.Run(() => _encryptDecryptIo.Decrypt(SourcePath, DestinationPath, Password)))
                await _dialogService.ShowMessageAsync("Decryption Success", "All your data is now decrypted");
            else
                await
                    _dialogService.ShowMessageAsync("Decryption Failure",
                        "Files could not be encrypted!\n\nCheck Logs for Information");
            IsDecryptionButtonEnabled = true;
        }

        /// <summary>
        ///     Called when SourcePathCommand RelayCommand is called, which opens a BrowseFolder-Dialog to set the
        ///     source path.
        /// </summary>
        private void OnSourcePathCommand()
        {
            var toSet = _dialogService.ShowOpenDirectoryDialog("Select source directory",
                Constants.Directories.GET_PERSONAL_FOLDER());
            if (string.IsNullOrWhiteSpace(toSet)) return;
            SourcePath = toSet;
        }

        /// <summary>
        ///     Called when DestPathCommand RelayCommand is called, which opens a BrowserFolder-Dialog to set the
        ///     destination path.
        /// </summary>
        private void OnDestPathCommand()
        {
            var toSet = _dialogService.ShowOpenDirectoryDialog("Select destination directory",
                Constants.Directories.GET_PERSONAL_FOLDER());
            if (string.IsNullOrWhiteSpace(toSet)) return;
            DestinationPath = toSet;
        }

        /// <summary>
        ///     Validations this instance's textboxes.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> Validation()
        {
            if (string.IsNullOrWhiteSpace(SourcePath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.SOURCE_IS_EMPTY_OR_WHITESPACE);
                return false;
            }
            if (string.IsNullOrWhiteSpace(DestinationPath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.DEST_IS_EMPTY_OR_WHITESPACE);
                return false;
            }
            if (SourcePath.Equals(DestinationPath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.SOURCE_IS_SAME_AS_DEST);
                return false;
            }
            if (!Directory.Exists(SourcePath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.SOURCE_IS_NOT_VALID);

                return false;
            }
            if (!Directory.Exists(DestinationPath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.DEST_IS_NOT_VALID);

                return false;
            }
            if (string.IsNullOrWhiteSpace(Password))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.PASSWORD_IS_NULL_OR_WHITESPACE);

                return false;
            }
            return true;
        }
    }
}