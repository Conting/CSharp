﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using Common.Commons;
using Common.Converter;
using Common.Helpers;
using Common.Model;
using Common.Model.Testing;
using Common.Services;
using Common.Services.Commands;
using Common.Services.Crypto;
using Common.Services.Serializer;
using MahApps.Metro.Controls.Dialogs;

namespace Common.ViewModel
{
    /// <summary>
    ///     The DecryptionViewModel's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class DatabaseViewModel : ViewModelBase
    {
        private readonly BinarySerializer _binarySerializer = new BinarySerializer();
        private readonly ListConverter _listConverter = new ListConverter();

        private readonly AbstractSymmetricCryptoService _abstractSymmetricCryptoService =
            new AbstractSymmetricCryptoService();

        private CreateLoginViewModel _createLoginViewModel;
        private IDialogService _dialogService;
        private EditLoginViewModel _editLoginViewModel;
        private bool _isCreateLoginInfoFlyoutOpen;
        private bool _isEditLoginInfoFlyoutOpen;
        private bool _isUserInformationFlyoutOpen;

        private string _lastUsedPath;
        private int _selectedRowIndex;
        private User _selectedUser;
        private UserInformationViewModel _userInformationViewModel;
        private ObservableCollection<User> _users = new ObservableCollection<User>();
        private readonly string key = "64Hi5SjGSq4J3Qj6jm8yD";

        /// <summary>
        ///     Initializes a new instance of the <see cref="DatabaseViewModel" /> class.
        /// </summary>
        /// <param name="dialogService">The dialog service.</param>
        public DatabaseViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;
            Initializate();
        }

        /// <summary>
        ///     Gets or sets the CreateLoginInfo command.
        /// </summary>
        /// <value>
        ///     The CreateLoginInfo command.
        /// </value>
        public RelayCommand CreateLoginInfo { get; set; }

        /// <summary>
        ///     Gets or sets the EditLoginInfo command.
        /// </summary>
        /// <value>
        ///     The EditLoginInfo command.
        /// </value>
        public RelayCommand EditLoginInfo { get; set; }

        /// <summary>
        ///     Gets or sets the ShowUserInformationCommand command.
        /// </summary>
        /// <value>
        ///     The ShowUserInformationCommand command.
        /// </value>
        public RelayCommand ShowUserInformationCommand { get; set; }

        /// <summary>
        ///     Gets or sets the DeleteLoginInfo command.
        /// </summary>
        /// <value>
        ///     The DeleteLoginInfo command.
        /// </value>
        public RelayCommand DeleteLoginInfo { get; set; }

        /// <summary>
        ///     Gets or sets the SaveLoginInfo command.
        /// </summary>
        /// <value>
        ///     The SaveLoginInfo command.
        /// </value>
        public RelayCommand SaveLoginInfo { get; set; }

        /// <summary>
        ///     Gets or sets the NewDatabase command.
        /// </summary>
        /// <value>
        ///     The NewDatabase command.
        /// </value>
        public RelayCommand NewDatabase { get; set; }

        /// <summary>
        ///     Gets or sets the LoadDatabase command.
        /// </summary>
        /// <value>
        ///     The LoadDatabase command.
        /// </value>
        public RelayCommand LoadDatabase { get; set; }

        /// <summary>
        ///     Gets or sets the dialog service.
        /// </summary>
        /// <value>
        ///     The dialog service.
        /// </value>
        public IDialogService DialogService
        {
            get { return _dialogService; }
            set
            {
                if (value.Equals(_dialogService)) return;
                _dialogService = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the last used database path.
        /// </summary>
        /// <value>
        ///     The last used database path.
        /// </value>
        public string LastUsedPath
        {
            get { return _lastUsedPath; }
            set
            {
                if (value.Equals(_lastUsedPath)) return;
                _lastUsedPath = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the UserInformationViewModel.
        /// </summary>
        /// <value>
        ///     The UserInformationViewModel.
        /// </value>
        public UserInformationViewModel UserInformationViewModel
        {
            get { return _userInformationViewModel; }
            set
            {
                if (value.Equals(_userInformationViewModel)) return;
                _userInformationViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the EditLoginViewModel.
        /// </summary>
        /// <value>
        ///     The EditLoginViewModel.
        /// </value>
        public EditLoginViewModel EditLoginViewModel
        {
            get { return _editLoginViewModel; }
            set
            {
                if (value.Equals(_editLoginViewModel)) return;
                _editLoginViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance's edit login information flyout is open.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance's edit login information flyout is open; otherwise, <c>false</c>.
        /// </value>
        public bool IsEditLoginInfoFlyoutOpen
        {
            get { return _isEditLoginInfoFlyoutOpen; }
            set
            {
                if (value.Equals(_isEditLoginInfoFlyoutOpen)) return;
                _isEditLoginInfoFlyoutOpen = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        ///     Gets or sets the CreateLoginViewModel.
        /// </summary>
        /// <value>
        ///     The CreateLoginViewModel.
        /// </value>
        public CreateLoginViewModel CreateLoginViewModel
        {
            get { return _createLoginViewModel; }
            set
            {
                if (value.Equals(_createLoginViewModel)) return;
                _createLoginViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the selected user in database.
        /// </summary>
        /// <value>
        ///     The selected user.
        /// </value>
        public User SelectedUser
        {
            get { return _selectedUser; }
            set
            {
                if (value == _selectedUser) return;
                _selectedUser = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the index of the selected row.
        /// </summary>
        /// <value>
        ///     The index of the selected row.
        /// </value>
        public int SelectedRowIndex
        {
            get { return _selectedRowIndex; }
            set
            {
                if (value == _selectedRowIndex) return;
                _selectedRowIndex = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance's user information flyout is open.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance's user information flyout is open; otherwise, <c>false</c>.
        /// </value>
        public bool IsUserInformationFlyoutOpen
        {
            get { return _isUserInformationFlyoutOpen; }
            set
            {
                if (value.Equals(_isUserInformationFlyoutOpen)) return;
                _isUserInformationFlyoutOpen = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance's create login information flyout is open.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance's create login information flyout is open; otherwise, <c>false</c>.
        /// </value>
        public bool IsCreateLoginInfoFlyoutOpen
        {
            get { return _isCreateLoginInfoFlyoutOpen; }
            set
            {
                if (value.Equals(_isCreateLoginInfoFlyoutOpen)) return;
                _isCreateLoginInfoFlyoutOpen = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the login list displayed in database.
        /// </summary>
        /// <value>
        ///     The users.
        /// </value>
        public ObservableCollection<User> Users
        {
            get { return _users; }
            set
            {
                if (value.Equals(_users)) return;
                _users = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Called when EditLoginInfo command is called, which opens the edit login flyout.
        /// </summary>
        private async void OnEditLoginViewCommand()
        {
            if ((SelectedUser != null) || (SelectedRowIndex != -1))
            {
                EditLoginViewModel = new EditLoginViewModel(() => IsEditLoginInfoFlyoutOpen = false, SelectedUser, this,
                    SelectedRowIndex, _dialogService);
                IsEditLoginInfoFlyoutOpen = true;
            }
            else
            {
                await DialogService.ShowMessageAsync("No Login selected", "Please select one");
            }
        }

        /// <summary>
        ///     Called when CreateLoginInfo relay command is called, which opens the
        ///     create login flyout.
        /// </summary>
        private async void OnNewLoginViewCommand()
        {
            if (string.IsNullOrWhiteSpace(LastUsedPath))
            {
                await _dialogService.ShowMessageAsync("Error", "Please create a Database first");
                return;
            }
            IsCreateLoginInfoFlyoutOpen = true;
        }

        /// <summary>
        ///     Initialization method for this instance.
        /// </summary>
        private void Initializate()
        {
            LoadDatabase = new RelayCommand(_ => OnLoadDatabaseCommand());
            NewDatabase = new RelayCommand(_ => OnNewDatabaseCommand());


            CreateLoginViewModel = new CreateLoginViewModel(() => IsCreateLoginInfoFlyoutOpen = false, this,
                _dialogService);
            UserInformationViewModel = new UserInformationViewModel(() => IsUserInformationFlyoutOpen = false,
                _selectedUser);
            CreateLoginInfo = new RelayCommand(_ => OnNewLoginViewCommand());
            EditLoginInfo = new RelayCommand(_ => OnEditLoginViewCommand());
            DeleteLoginInfo = new RelayCommand(_ => OnDeleteLoginCommand());
            SaveLoginInfo = new RelayCommand(_ => OnSaveLoginCommand());
            ShowUserInformationCommand = new RelayCommand(_ => OnShowUserCommand());
            Users = new ObservableCollection<User>();
            SelectedRowIndex = -1;
        }

        /// <summary>
        ///     Called when ShowUserInformationCommand relay command is called, which opens
        ///     a Flyout with login information.
        /// </summary>
        private void OnShowUserCommand()
        {
            UserInformationViewModel.SetInformation(_selectedUser);
            IsUserInformationFlyoutOpen = true;
        }

        /// <summary>
        ///     Called when SaveLoginInfo relay command is called.
        /// </summary>
        private void OnSaveLoginCommand()
        {
            SaveDatabase();
        }

        /// <summary>
        ///     Called when NewDatabase relay command is called.
        /// </summary>
        private async void OnNewDatabaseCommand()
        {
            await CreateDatabase();
        }

        /// <summary>
        ///     Saves the database in a file.
        /// </summary>
        public async void SaveDatabase()
        {
            var toSerialize = _listConverter.ConvertObservableToList(Users, new List<User>());
            if ((LastUsedPath != null) && File.Exists(LastUsedPath))
            {
                if (_binarySerializer.Serialize(toSerialize, LastUsedPath))
                {
                    _abstractSymmetricCryptoService.AES_Encrypt(LastUsedPath, key, ".temp");

                    File.Replace(LastUsedPath + ".temp", LastUsedPath, null);


                    await DialogService.ShowMessageAsync("Success", "Database is now safed");
                    InfoUpdater.Provider.Title = "SecSuite: " + Path.GetFileName(LastUsedPath);
                    InfoUpdater.Provider.Status = "Database saved";
                    InfoUpdater.Provider.Changes = false;
                }
                else
                {
                    await DialogService.ShowMessageAsync("Error", "Check Logs for more Information");
                }
            }
            else
            {
                await DialogService.ShowMessageAsync("Caution", "Please create a Database file!");
                if (await CreateDatabase())
                    if (_binarySerializer.Serialize(toSerialize, LastUsedPath))
                        await DialogService.ShowMessageAsync("Success", "Database is now safed");
                    else
                        await DialogService.ShowMessageAsync("Error", "Check Logs for more Information");
                else
                    await DialogService.ShowMessageAsync("Error", "Check Logs for more Information");
            }
        }

        /// <summary>
        ///     Creates the database and saves it in a file.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> CreateDatabase()
        {
            var toSet = DialogService.ShowSaveFileDialog("Create Database", Constants.Directories.GET_PERSONAL_FOLDER(),
                "ConBase", ".conBase");
            if (string.IsNullOrWhiteSpace(toSet)) return false;
            var toSerialize = new List<User>();
            if (!_binarySerializer.Serialize(toSerialize, toSet)) return false;
            LastUsedPath = toSet;
            _abstractSymmetricCryptoService.AES_Encrypt(toSet, key, ".temp");
            Users = _listConverter.ConvertListToObservable(toSerialize, new ObservableCollection<User>());
            ClearFlyouts();

            File.Replace(LastUsedPath + ".temp", LastUsedPath, null);

            InfoUpdater.Provider.Title = "SecSuite: " + Path.GetFileName(toSet);
            InfoUpdater.Provider.Status = "Database created: " + Path.GetFileName(toSet);
            await DialogService.ShowMessageAsync("Success", "Database is now created");

            return true;
        }

        /// <summary>
        ///     Called when LoadDatabase relay command is called, which opens a database.
        /// </summary>
        private async void OnLoadDatabaseCommand()
        {
            await OpenDatabase();
        }

        /// <summary>
        ///     Opens the database from file.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> OpenDatabase()
        {
            var toSet = DialogService.ShowOpenFileDialog("Open Database", Constants.Directories.GET_PERSONAL_FOLDER(),
                "ConBase",
                ".conbase");
            if (string.IsNullOrWhiteSpace(toSet)) return false;
            if (!_abstractSymmetricCryptoService.AES_Decrypt(toSet, key, Path.GetDirectoryName(toSet)))
            {
                await DialogService.ShowMessageAsync("Error", "Database could not be opened!");
                InfoUpdater.Provider.Status = "Failed to open: " + Path.GetFileName(toSet);
                return false;
            }
            var temp = Path.GetDirectoryName(toSet) + @"\" + Path.GetFileNameWithoutExtension(toSet);
            var database = _binarySerializer.Deserialize<List<User>>(temp);
            if (database == null)
            {
                File.Delete(temp);
                await DialogService.ShowMessageAsync("Error", "Database could not be opened!");
                InfoUpdater.Provider.Status = "Failed to open: " + Path.GetFileName(toSet);
                return false;
            }
            LastUsedPath = toSet;
            File.Delete(temp);
            Users = _listConverter.ConvertListToObservable(database, new ObservableCollection<User>());
            var titleToSet = "SecSuite: " + Path.GetFileName(toSet);
            InfoUpdater.Provider.Title = titleToSet;
            InfoUpdater.Provider.Status = "Database opened: " + Path.GetFileName(toSet);
            await DialogService.ShowMessageAsync("Database loading Success", "The Database is now loaded");
            return true;
        }

        /// <summary>
        ///     Called when DeleteLoginInfo Relay Command is called, which deletes a user in database list.
        /// </summary>
        private async void OnDeleteLoginCommand()
        {
            if (SelectedUser == null)
            {
                await DialogService.ShowMessageAsync("Error!", "No Login selected!");
                return;
            }
            if (!await
                Task.Run(
                    () =>
                        DialogService.AskQuestionAsync("Caution!", "Do you really want to delete this Login?").Result ==
                        MessageDialogResult.Affirmative)) return;
            if (RemoveUser(SelectedUser))
            {
                await DialogService.ShowMessageAsync("Success!", "Login is now deleted!");
                InfoUpdater.Provider.Changes = true;
                InfoUpdater.Provider.Title = InfoUpdater.Provider.Title + "*";
            }
            else
            {
                await DialogService.ShowMessageAsync("Error!", "Login could not be deleted!");
            }
        }


        /// <summary>
        ///     User sample method for testing porpurses
        /// </summary>
        /// <returns></returns>
        public ObservableCollection<User> Samples()
        {
            var geneator = new UserGenerator();
            return geneator.Samples();
        }

        /// <summary>
        ///     Adds the user to database list.
        /// </summary>
        /// <param name="user">User Object</param>
        /// <returns></returns>
        public bool AddUser(User user)
        {
            Users.Add(user);
            IsCreateLoginInfoFlyoutOpen = false;
            return Users.Contains(user);
        }

        /// <summary>
        ///     Replaces the user in database at the selected row index.
        /// </summary>
        /// <param name="user">User Object</param>
        /// <param name="rowIndex">Row Index of the User Obejct.</param>
        /// <returns></returns>
        public bool ReplaceUser(User user, int rowIndex)
        {
            Users.RemoveAt(rowIndex);
            Users.Insert(rowIndex, user);
            return Users.Contains(user);
        }

        /// <summary>
        ///     Removes the user from the database list.
        /// </summary>
        /// <param name="user">User Object</param>
        /// <returns></returns>
        public bool RemoveUser(User user)
        {
            if (SelectedRowIndex != -1)
            {
                Users.RemoveAt(SelectedRowIndex);
                return !Users.Contains(user);
            }
            return false;
        }

        /// <summary>
        ///     Clears the EditLoginViewModel, CreateLoginViewModel, UserInformationViewModel textboxes.
        /// </summary>
        public void ClearFlyouts()
        {
            EditLoginViewModel?.Clear();
            CreateLoginViewModel?.Clear();
            UserInformationViewModel?.Clear();
        }
    }
}