﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Commons;
using Common.Helpers;
using Common.Model;
using Common.Services;
using Common.Services.Commands;
using Common.Services.Pwgenerator;

namespace Common.ViewModel
{
    /// <summary>
    ///     The CreateLoginViewModel's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class CreateLoginViewModel : ViewModelBase
    {
        private readonly PwGenerator _pwGenerator = new PwGenerator();
        private DatabaseViewModel _databaseViewModel;
        private IDialogService _dialogService;
        private string _password;
        private string _title;
        private User _user;
        private string _username;
        private string _website;

        /// <summary>
        ///     Initializes a new instance of the <see cref="CreateLoginViewModel" /> class.
        /// </summary>
        /// <param name="closeAction">The close action.</param>
        /// <param name="databaseViewModel">The database view model.</param>
        /// <param name="dialogService">The dialog service.</param>
        public CreateLoginViewModel(Action closeAction, DatabaseViewModel databaseViewModel,
            IDialogService dialogService)
        {
            DialogService = dialogService;
            _databaseViewModel = databaseViewModel;
            Initialize();
        }

        /// <summary>
        ///     Gets or sets the AddUserCommand command.
        /// </summary>
        /// <value>
        ///     The AddUserCommand.
        /// </value>
        public RelayCommand AddUserCommand { get; set; }

        /// <summary>
        ///     Gets or sets the GeneratePasswordCommand command.
        /// </summary>
        /// <value>
        ///     The GeneratePasswordCommand.
        /// </value>
        public RelayCommand GeneratePasswordCommand { get; set; }

        /// <summary>
        ///     Gets or sets the dialog service.
        /// </summary>
        /// <value>
        ///     The dialog service.
        /// </value>
        public IDialogService DialogService
        {
            get { return _dialogService; }
            set
            {
                if (value.Equals(_dialogService)) return;
                _dialogService = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        /// <value>
        ///     The title.
        /// </value>
        public string Title
        {
            get { return _title; }
            set
            {
                if (value.Equals(_title)) return;
                _title = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the username.
        /// </summary>
        /// <value>
        ///     The username.
        /// </value>
        public string Username
        {
            get { return _username; }
            set
            {
                if (value.Equals(_username)) return;
                _username = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the password.
        /// </summary>
        /// <value>
        ///     The password.
        /// </value>
        public string Password
        {
            get { return _password; }
            set
            {
                if (value.Equals(_password)) return;
                _password = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the website.
        /// </summary>
        /// <value>
        ///     The website.
        /// </value>
        public string Website
        {
            get { return _website; }
            set
            {
                if (value.Equals(_website)) return;
                _website = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the login.
        /// </summary>
        /// <value>
        ///     The user object.
        /// </value>
        public User User
        {
            get { return _user; }
            set
            {
                if (value == _user) return;
                _user = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the DatabaseViewModel.
        /// </summary>
        /// <value>
        ///     The DatabaseViewModel.
        /// </value>
        public DatabaseViewModel DatabaseViewModel
        {
            get { return _databaseViewModel; }
            set
            {
                if (value == _databaseViewModel) return;
                _databaseViewModel = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Creates a user.
        /// </summary>
        /// <returns></returns>
        private User CreateUser()
        {
            var toReturnUser = new User
            {
                Title = _title,
                Username = _username,
                Password = _password,
                Website = _website,
                Modified = DateTime.Now,
                Created = DateTime.Now
            };
            return toReturnUser;
        }

        /// <summary>
        ///     Called when AddUserCommand relay command is called, which add a login to the database list.
        /// </summary>
        public async void OnAddUserCommand()
        {
            if (!await Task.Run(() => Validation().Result.Equals(true))) return;
            _databaseViewModel.AddUser(CreateUser());
            InfoUpdater.Provider.Changes = true;
            InfoUpdater.Provider.Title = InfoUpdater.Provider.Title + "*";
            Clear();
        }


        /// <summary>
        ///     Initialization method for this instance.
        /// </summary>
        public void Initialize()
        {
            AddUserCommand = new RelayCommand(_ => OnAddUserCommand());
            GeneratePasswordCommand = new RelayCommand(_ => OnGeneratePassword());
            Website = @"http://example.com/";
        }

        /// <summary>
        ///     Called when GeneratePasswordCommand relay command is called,
        ///     which generates a 16 char long secure password that is set to password.
        /// </summary>
        private void OnGeneratePassword()
        {
            var pwGeneratorInstance = PwGeneratorViewModel.Instance;
            var initList = new List<string>();
            if (pwGeneratorInstance.LCase)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_LCASE);
            if (pwGeneratorInstance.UCase)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_UCASE);
            if (pwGeneratorInstance.Numeric)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_NUMERIC);
            if (pwGeneratorInstance.Special)
                initList.Add(Constants.Passwords.PASSWORD_CHARS_SPECIAL);
            var toSet = _pwGenerator.GeneratePassword(pwGeneratorInstance.PwLength, initList);
            Password = toSet;
        }

        /// <summary>
        ///     Validations this instance's textboxes.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> Validation()
        {
            if (string.IsNullOrWhiteSpace(Title))
            {
                await DialogService.ShowMessageAsync("Title cannot be empty!", "Please fill textbox out!");
                return false;
            }
            if (string.IsNullOrWhiteSpace(Username))
            {
                await DialogService.ShowMessageAsync("Username cannot be empty!", "Please fill textbox out!");

                return false;
            }
            if (string.IsNullOrWhiteSpace(Password))
            {
                await DialogService.ShowMessageAsync("Password cannot be empty!", "Please fill passwordbox out!");

                return false;
            }
            if (string.IsNullOrWhiteSpace(Website))
            {
                await DialogService.ShowMessageAsync("Website cannot be empty!", "Please fill textbox out!");

                return false;
            }
            return true;
        }

        /// <summary>
        ///     Clears this instance's textboxes.
        /// </summary>
        public void Clear()
        {
            Title = "";
            Password = "";
            Username = "";
            Website = "";
        }
    }
}