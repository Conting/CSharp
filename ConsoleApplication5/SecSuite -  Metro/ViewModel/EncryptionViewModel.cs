﻿using System.IO;
using System.Threading.Tasks;
using Common.Commons;
using Common.Core;
using Common.Helpers;
using Common.Services.Commands;
using MahApps.Metro.Controls.Dialogs;

namespace Common.ViewModel
{
    /// <summary>
    ///     The EncryptionViewModel's viewModel.
    /// </summary>
    /// <seealso cref="Common.ViewModel.ViewModelBase" />
    public class EncryptionViewModel : ViewModelBase
    {
        private readonly IDialogService _dialogService;
        private readonly EncryptDecryptIo _encryptDecryptIo;
        private string _destinationPath;
        private bool _isEncryptionButtonEnabled;
        private string _password;
        private string _passwordConfirm;
        private string _sourcePath;

        /// <summary>
        ///     Initializes a new instance of the <see cref="EncryptionViewModel" /> class.
        /// </summary>
        /// <param name="dialogService">The dialog service.</param>
        public EncryptionViewModel(IDialogService dialogService)
        {
            IsEncryptionButtonEnabled = true;
            EncryptCommand = new RelayCommand(_ => OnEncryptCommand());
            SetDestPath = new RelayCommand(_ => OnSetDestPathCommand());
            SetSourcePath = new RelayCommand(_ => OnSetSourcePathCommand());
            _encryptDecryptIo = new EncryptDecryptIo();
            _dialogService = dialogService;
        }

        /// <summary>
        ///     Gets or sets the encrypt command.
        /// </summary>
        /// <value>
        ///     The encrypt command.
        /// </value>
        public RelayCommand EncryptCommand { get; set; }

        /// <summary>
        ///     Gets or sets the SetSourcePath Command.
        /// </summary>
        /// <value>
        ///     The SetSourcePath Command.
        /// </value>
        public RelayCommand SetSourcePath { get; set; }

        /// <summary>
        ///     Gets or sets the SetDestPath Command.
        /// </summary>
        /// <value>
        ///     The SetDestPath Command.
        /// </value>
        public RelayCommand SetDestPath { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance's encryption button is enabled.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance's encryption button is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool IsEncryptionButtonEnabled
        {
            get { return _isEncryptionButtonEnabled; }
            set
            {
                if (value.Equals(_isEncryptionButtonEnabled)) return;
                _isEncryptionButtonEnabled = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the password confirmation string.
        /// </summary>
        /// <value>
        ///     The password confirmation string.
        /// </value>
        public string PasswordConfirm
        {
            get { return _passwordConfirm; }
            set
            {
                if (value.Equals(_passwordConfirm)) return;
                _passwordConfirm = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the password.
        /// </summary>
        /// <value>
        ///     The password.
        /// </value>
        public string Password
        {
            get { return _password; }
            set
            {
                if (value.Equals(_password)) return;
                _password = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the source path to encrypt.
        /// </summary>
        /// <value>
        ///     The source path.
        /// </value>
        public string SourcePath
        {
            get { return _sourcePath; }
            set
            {
                if (value.Equals(_sourcePath)) return;
                _sourcePath = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Gets or sets the destination path, where the encrypted files are saved.
        /// </summary>
        /// <value>
        ///     The destination path.
        /// </value>
        public string DestinationPath
        {
            get { return _destinationPath; }
            set
            {
                if (value.Equals(_destinationPath)) return;
                _destinationPath = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        ///     Called when SetSourcePath RelayCommand is called, which opens a ShowOpenDirectoryDialog.
        /// </summary>
        private void OnSetSourcePathCommand()
        {
            var toSet = _dialogService.ShowOpenDirectoryDialog("Select source directory",
                Constants.Directories.GET_PERSONAL_FOLDER());
            if (string.IsNullOrWhiteSpace(toSet)) return;
            SourcePath = toSet;
        }

        /// <summary>
        ///     Called when SetDestPath RelayCommand is called, which opens a ShowOpenDirectoryDialog.
        /// </summary>
        private void OnSetDestPathCommand()
        {
            var toSet = _dialogService.ShowOpenDirectoryDialog("Select destination directory",
                Constants.Directories.GET_PERSONAL_FOLDER());
            if (string.IsNullOrWhiteSpace(toSet)) return;
            DestinationPath = toSet;
        }

        /// <summary>
        ///     Called when EncryptCommand RelayCommand is called, which starts the encryption process.
        /// </summary>
        private async void OnEncryptCommand()
        {
            if (await Task.Run(() => Validation().Result.Equals(false))) return;
            IsEncryptionButtonEnabled = false;


            if (!await Task.Run(() => _dialogService.AskQuestionAsync(Constants.WARNINGS.ENCRYPTION_WARNING,
                                              Constants.QUESTIONS.ENCRYPTION_QUESTION +
                                              "\n\n Your Data will be encryted with a AES 256 Bit Key, PKCS7 Padding in CFB mode!")
                                          .Result ==
                                      MessageDialogResult.Affirmative)) return;
            if (await Task.Run(() => _encryptDecryptIo.Encrypt(SourcePath, DestinationPath, Password)))
            {
                await _dialogService.ShowMessageAsync("Encryption Success", "All your data is now encrypted");
                IsEncryptionButtonEnabled = true;
            }
            else
            {
                await _dialogService.ShowMessageAsync("Encryption Failure", "All your data is not encrypted!");

                IsEncryptionButtonEnabled = true;
            }
        }

        /// <summary>
        ///     Validations for the instance's textboxes.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> Validation()
        {
            if (string.IsNullOrWhiteSpace(SourcePath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.SOURCE_IS_EMPTY_OR_WHITESPACE);
                return false;
            }
            if (string.IsNullOrWhiteSpace(DestinationPath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.DEST_IS_EMPTY_OR_WHITESPACE);
                return false;
            }
            if (SourcePath.Equals(DestinationPath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.SOURCE_IS_SAME_AS_DEST);
                return false;
            }
            if (!Directory.Exists(SourcePath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.SOURCE_IS_NOT_VALID);

                return false;
            }
            if (!Directory.Exists(DestinationPath))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.DEST_IS_NOT_VALID);

                return false;
            }
            if (string.IsNullOrWhiteSpace(Password))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.PASSWORD_IS_NULL_OR_WHITESPACE);

                return false;
            }
            if (string.IsNullOrWhiteSpace(PasswordConfirm))
            {
                await
                    _dialogService.ShowMessageAsync(Constants.Errors.ENCRYPTION_ERROR,
                        Constants.Errors.PASSWORD_IS_NULL_OR_WHITESPACE);
                return false;
            }
            if (Password.Equals(PasswordConfirm))
            {
                return true;
            }
            return false;
        }
    }
}